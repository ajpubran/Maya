Jun Mayab
===================

Repositorio oficial Jun Mayab.
------------------------------


Aquí encontraras los archivos Apk oficiales para la plataforma Android.

# Paso 1.
> **Descarga** el archivo Apk a tu dispositivo integrado Android.

# Paso 2.
> Abre el archivo y habilita la opción instalar de origenes desconocidos.
>
> Android **>** Ajustes **>** Seguridad **>** Administración de dispositivos 
>
> **Habilita** *Origenes Desconocidos*

**Nota**: Instala archivos apk de la página o repositorio oficial 
de un desarrollador confiable el cual respete tu privacidad.

# Paso 3.
> Si deseas puedes deshabilidar la instalación de origenes desconocidos.
>
> Android **>** Ajustes **>** Seguridad **>** Administración de dispositivos 
>
> **Deshabilita** *Origenes Desconocidos*

# Paso 4
> **Disfruta** tu aplicación.
